$(function () {
  let country = "Afghanistan";
  map();
  document.getElementById("filter1").addEventListener("click", function () {
    country = $("#country option:selected").val();
    map();
  });
  function map() {
    let allData = [
      {
        year: 2021,
        mouth: 1,
        type: "0",
        data: 56506695.12,
        data2: 4.978913e7,
      },
      {
        year: 2021,
        mouth: 2,
        type: "1",
        data: 32143617.84,
        data2: 3.506963e7,
      },
      {
        year: 2021,
        mouth: 3,
        type: "1",
        data: 52671691.07,
        data2: 3.755372e7,
      },
      {
        year: 2021,
        mouth: 4,
        type: "2",
        data: 46371203.77,
        data2: 4.508753e7,
      },
      {
        year: 2021,
        mouth: 5,
        type: "1",
        data: 51300066.18,
        data2: 5.430761e7,
      },
      {
        year: 2021,
        mouth: 6,
        type: "2",
        data: 53583164.04,
        data2: 5.399435e7,
      },
      {
        year: 2021,
        mouth: 7,
        type: "0",
        data: 62746372.87,
        data2: 5.744038e7,
      },
      {
        year: 2021,
        mouth: 8,
        type: "1",
        data: 58464024.95,
        data2: 5.548484e7,
      },
      {
        year: 2021,
        mouth: 9,
        type: "2",
        data: 49352837.57,
        data2: 5.302529e7,
      },
      {
        year: 2021,
        mouth: 10,
        type: "3",
        data: 49045926.45,
        data2: 5.400274e7,
      },
      {
        year: 2021,
        mouth: 11,
        type: "1",
        data: 78500717.34,
        data2: 6.284812e7,
      },
      {
        year: 2021,
        mouth: 12,
        type: "0",
        data: 51207919.84,
        data2: 6.137937e7,
      },
      {
        year: 2021,
        mouth: 1,
        type: "2",
        data: 49733614.54,
        data2: 4.73246e7,
      },
      {
        year: 2021,
        mouth: 2,
        type: "1",
        data: 28285824,
        data2: 3.128552e7,
      },
      {
        year: 2021,
        mouth: 3,
        type: "2",
        data: 40927986.61,
        data2: 3.032183e7,
      },
      {
        year: 2021,
        mouth: 4,
        type: "1",
        data: 34277165.19,
        data2: 3.62262e7,
      },
      {
        year: 2021,
        mouth: 5,
        type: "0",
        data: 39566410.11,
        data2: 3.763036e7,
      },
      {
        year: 2021,
        mouth: 6,
        type: "1",
        data: 48040128.03,
        data2: 3.586835e7,
      },
      {
        year: 2021,
        mouth: 7,
        type: "2",
        data: 62129505.94,
        data2: 5.557498e7,
      },
      {
        year: 2021,
        mouth: 8,
        type: "2",
        data: 57914716.22,
        data2: 5.868255e7,
      },
      {
        year: 2021,
        mouth: 9,
        type: "1",
        data: 48765240.68,
        data2: 4.69274e7,
      },
      {
        year: 2021,
        mouth: 10,
        type: "1",
        data: 42042160.33,
        data2: 4.81016e7,
      },
      {
        year: 2021,
        mouth: 11,
        type: "0",
        data: 75849029.23,
        data2: 6.229651e7,
      },
      {
        year: 2021,
        mouth: 12,
        type: "1",
        data: 46974125.26,
        data2: 5.880157e7,
      },
      {
        year: 2021,
        mouth: 1,
        type: "0",
        data: 655749.48,
        data2: 6.854559e5,
      },
      {
        year: 2021,
        mouth: 2,
        type: "0",
        data: 656827.97,
        data2: 6.792206e5,
      },
      {
        year: 2021,
        mouth: 3,
        type: "0",
        data: 584536.12,
        data2: 6.58254e5,
      },
      {
        year: 2021,
        mouth: 4,
        type: "2",
        data: 601036.95,
        data2: 5.533392e5,
      },
      {
        year: 2021,
        mouth: 5,
        type: "2",
        data: 537586.41,
        data2: 5.309395e5,
      },
      {
        year: 2021,
        mouth: 6,
        type: "2",
        data: 545797.64,
        data2: 4.852669e5,
      },
      {
        year: 2021,
        mouth: 7,
        type: "2",
        data: 486870.28,
        data2: 5.067846e5,
      },
      {
        year: 2021,
        mouth: 8,
        type: "1",
        data: 492548.6,
        data2: 5.737141e5,
      },
      {
        year: 2021,
        mouth: 9,
        type: "1",
        data: 522980.04,
        data2: 5.393724e5,
      },
      {
        year: 2021,
        mouth: 10,
        type: "1",
        data: 629826.18,
        data2: 5.919242e5,
      },
      {
        year: 2021,
        mouth: 11,
        type: "2",
        data: 1405577.16,
        data2: 6.662492e5,
      },
      {
        year: 2021,
        mouth: 12,
        type: "2",
        data: 588495.37,
        data2: 1.252812e6,
      },
      {
        year: 2021,
        mouth: 1,
        type: "居民",
        data: 34659723.77,
        data2: 3.653325e7,
      },
      {
        year: 2021,
        mouth: 2,
        type: "居民",
        data: 37798585.87,
        data2: 3.663951e7,
      },
      {
        year: 2021,
        mouth: 3,
        type: "居民",
        data: 25264555.34,
        data2: 3.092726e7,
      },
      {
        year: 2021,
        mouth: 4,
        type: "居民",
        data: 24925854.82,
        data2: 2.723387e7,
      },
      {
        year: 2021,
        mouth: 5,
        type: "居民",
        data: 19711122.26,
        data2: 1.500864e7,
      },
      {
        year: 2021,
        mouth: 6,
        type: "居民",
        data: 20220794.89,
        data2: 1.14282e7,
      },
      {
        year: 2021,
        mouth: 7,
        type: "居民",
        data: 24904615.71,
        data2: 2.396474e7,
      },
      {
        year: 2021,
        mouth: 8,
        type: "居民",
        data: 39131890.61,
        data2: 3.678047e7,
      },
      {
        year: 2021,
        mouth: 9,
        type: "居民",
        data: 40177481.33,
        data2: 4.128945e7,
      },
      {
        year: 2021,
        mouth: 10,
        type: "居民",
        data: 29896242.87,
        data2: 2.931177e7,
      },
      {
        year: 2021,
        mouth: 11,
        type: "居民",
        data: 27892767.79,
        data2: 1.88612e7,
      },
      {
        year: 2021,
        mouth: 12,
        type: "居民",
        data: 49720016.82,
        data2: 4.735282e7,
      },
      {
        year: 2021,
        mouth: 1,
        type: "农业",
        data: 543970.28,
        data2: 504275.463063,
      },
      {
        year: 2021,
        mouth: 2,
        type: "农业",
        data: 375964.37,
        data2: 476556.647308,
      },
      {
        year: 2021,
        mouth: 3,
        type: "农业",
        data: 508478.89,
        data2: 492825.452484,
      },
      {
        year: 2021,
        mouth: 4,
        type: "农业",
        data: 418687.02,
        data2: 392958.30751,
      },
      {
        year: 2021,
        mouth: 5,
        type: "农业",
        data: 495567.79,
        data2: 468237.616365,
      },
      {
        year: 2021,
        mouth: 6,
        type: "农业",
        data: 621244.24,
        data2: 648676.246408,
      },
      {
        year: 2021,
        mouth: 7,
        type: "农业",
        data: 769509.85,
        data2: 723023.920294,
      },
      {
        year: 2021,
        mouth: 8,
        type: "农业",
        data: 887391.38,
        data2: 843111.233879,
      },
      {
        year: 2021,
        mouth: 9,
        type: "农业",
        data: 897306.87,
        data2: 823723.469611,
      },
      {
        year: 2021,
        mouth: 10,
        type: "农业",
        data: 674226.43,
        data2: 750725.873934,
      },
      {
        year: 2021,
        mouth: 11,
        type: "农业",
        data: 955112.4,
        data2: 693572.255216,
      },
      {
        year: 2021,
        mouth: 12,
        type: "农业",
        data: 523695.46,
        data2: 584248.16383,
      },
      {
        year: 2021,
        mouth: 1,
        type: "售外省",
        data: 4738079.6,
        data2: 4.258049e6,
      },
      {
        year: 2021,
        mouth: 2,
        type: "售外省",
        data: 4437235.5,
        data2: 4.374106e6,
      },
      {
        year: 2021,
        mouth: 3,
        type: "售外省",
        data: 3658432,
        data2: 3.779838e6,
      },
      {
        year: 2021,
        mouth: 4,
        type: "售外省",
        data: 3338185,
        data2: 3.753966e6,
      },
      {
        year: 2021,
        mouth: 5,
        type: "售外省",
        data: 3870562.8,
        data2: 3.990618e6,
      },
      {
        year: 2021,
        mouth: 6,
        type: "售外省",
        data: 4056801.8,
        data2: 3.901051e6,
      },
      {
        year: 2021,
        mouth: 7,
        type: "售外省",
        data: 4436051.6,
        data2: 4.11702e6,
      },
      {
        year: 2021,
        mouth: 8,
        type: "售外省",
        data: 4471495,
        data2: 4.33344e6,
      },
      {
        year: 2021,
        mouth: 9,
        type: "售外省",
        data: 4102899.4,
        data2: 4.083217e6,
      },
      {
        year: 2021,
        mouth: 10,
        type: "售外省",
        data: 4181470.2,
        data2: 4.004165e6,
      },
      {
        year: 2021,
        mouth: 11,
        type: "售外省",
        data: 3680211.5,
        data2: 4.279472e6,
      },
      {
        year: 2021,
        mouth: 12,
        type: "售外省",
        data: 3793550.3,
        data2: 3.931424e6,
      },
    ];

    //获取某个value并去重
    function getValues(arr, key) {
      let newArr = [];
      for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
          if (arr[i][key] === arr[j][key]) {
            ++i;
          }
        }
        newArr.push(arr[i][key]);
      }
      return [...new Set(newArr)];
    }

    let legends = getValues(allData, "type");
    legends = ["1岁以下儿童人口", "25岁以下人口", "15至64岁人口", "总人口"];
    if (legends != "" && legends != null) {
      $("#types").empty()
      //$("#years").append("<option value=''>请选择</option>");
      for (var i = 0; i < legends.length; i++) {
        $("#types").append(`<option value='${i}'>${legends[i]}</option>`); //新增
      }
      $("#types option:eq(0)").attr("selected", "selected"); //选中第一个
    }

    function getData(data) {
      let lineData = [[], []];
      data.map(function (item) {
        // if (item.type == type) {
        lineData[0].push(item.total_population);
        // let deviation =
        //   Math.abs(item.predicted_population - item.total_population) /
        //   item.total_population;
        // console.log(deviation);
        // if (deviation > 0.05) {
        //   lineData[1].push({
        //     value: item.predicted_population,
        //     itemStyle: {
        //       color: "#F00",
        //       borderColor: "#fff",
        //       borderWidth: 2,
        //       shadowColor: "rgba(0, 0, 0, .3)",
        //       shadowBlur: 0,
        //       shadowOffsetY: 2,
        //       shadowOffsetX: 2,
        //     },
        //   });
        // } else {
        //   lineData[1].push({
        //     value: item.predicted_population,
        //     itemStyle: {
        //       color: "#00ca95",
        //       borderColor: "#fff",
        //       borderWidth: 2,
        //       shadowColor: "rgba(0, 0, 0, .3)",
        //       shadowBlur: 0,
        //       shadowOffsetY: 2,
        //       shadowOffsetX: 2,
        //     },
        //   });
        // }
        // }
      });
      return lineData;
    }
    axios({
      method: "get",
      url: "/map",
      params: {
        type: "0",
        country,
      },
    }).then((res) => {
      randerMap(getData(res.data), legends[0]);
    });
    // randerMap(getData(0), legends[0]);

    //筛选按钮
    document.getElementById("filter").addEventListener("click", function () {
      let checkType = $("#types option:selected").val();
      axios({
        method: "get",
        url: "/map",
        params: {
          type: checkType,
          country,
        },
      }).then((res) => {
        randerMap(getData(res.data), legends[checkType]);
      });
      //   randerMap(getData(checkType), checkType);
    });

    function randerMap(lineData, legends) {
      let legend = [
        `${legends}-真实值`,
        // `${legends}-预测值（预测值偏差超过5%的点为红色）`,
      ];
      console.log(legend, lineData, "--lineData");
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById("map_1"));
      option = {
        title: [
          {
            top: "0%",
            left: "center",
            text: "1950年-2021年该国人口变化趋势",

            textStyle: {
              color: "#fff",
              fontSize: "20",
              fontWeight: "normal",
            },
          },
        ],
        tooltip: {
          trigger: "axis",
          axisPointer: {
            lineStyle: {
              color: "#dddc6b",
            },
          },
          formatter: function (params) {
            return `<span  style='font-weight:bold;line-height:28px;color:#FFF'>${params[0].name}</span></br>
                        ${params[0].marker}${params[0].seriesName}<span style='padding-left:10px;color:#FFF'>${params[0].data}</span>`;
          },
        },
        legend: {
          top: "4%",
          // left: '20%',
          data: legend,
          textStyle: {
            color: "rgba(255,255,255,.8)",
            fontSize: "12",
          },
        },
        grid: {
          left: "5%",
          top: "10%",
          right: "5%",
          bottom: "5%",
          containLabel: true,
        },

        xAxis: [
          {
            type: "category",
            boundaryGap: false,
            axisLabel: {
              rotate: -45,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: 12,
              },
            },
            axisLine: {
              lineStyle: {
                color: "rgba(255,255,255,.2)",
              },
            },

            data: [
              // "2011",
              // "2012",
              // "2013",
              // "2014",
              // "2015",
              // "2016",
              // "2017",
              // "2018",
              // "2019",
              // "2020",
              // "2021",
            ],
          },
          {
            axisPointer: { show: false },
            axisLine: { show: false },
            position: "bottom",
            offset: 20,
          },
        ],

        yAxis: [
          {
            type: "value",
            axisTick: { show: false },
            axisLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
            axisLabel: {
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: 12,
              },
            },

            splitLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
          },
        ],
        series: [
          {
            name: legend[0],
            type: "line",
            // smooth: true, //是否平滑
            showAllSymbol: true,
            // symbol: 'image://./static/images/guang-circle.png',
            symbol: "circle",
            symbolSize: 16,
            lineStyle: {
              normal: {
                color: "#6c50f3",
                shadowColor: "rgba(0, 0, 0, .3)",
                shadowBlur: 0,
                shadowOffsetY: 5,
                shadowOffsetX: 5,
              },
            },
            label: {
              show: false,
              position: "top",
              textStyle: {
                color: "#6c50f3",
              },
            },
            itemStyle: {
              color: "#6c50f3",
              borderColor: "#fff",
              borderWidth: 2,
              shadowColor: "rgba(0, 0, 0, .3)",
              shadowBlur: 0,
              shadowOffsetY: 2,
              shadowOffsetX: 2,
            },
            tooltip: {
              show: true,
            },
            areaStyle: {
              normal: {
                color: new echarts.graphic.LinearGradient(
                  0,
                  0,
                  0,
                  1,
                  [
                    {
                      offset: 0,
                      color: "rgba(108,80,243,0.3)",
                    },
                    {
                      offset: 1,
                      color: "rgba(108,80,243,0)",
                    },
                  ],
                  false
                ),
                shadowColor: "rgba(108,80,243, 0.9)",
                shadowBlur: 20,
              },
            },
            data: lineData[0],
          },
          {
            name: legend[1],
            type: "line",
            // smooth: true, //是否平滑
            showAllSymbol: true,
            // symbol: 'image://./static/images/guang-circle.png',
            symbol: "circle",
            symbolSize: 16,
            lineStyle: {
              normal: {
                color: "#00ca95",
                shadowColor: "rgba(0, 0, 0, .3)",
                shadowBlur: 0,
                shadowOffsetY: 5,
                shadowOffsetX: 5,
              },
            },
            label: {
              show: false,
              position: "top",
              textStyle: {
                color: "#00ca95",
              },
            },

            itemStyle: {
              color: "#00ca95",
              borderColor: "#fff",
              borderWidth: 2,
              shadowColor: "rgba(0, 0, 0, .3)",
              shadowBlur: 0,
              shadowOffsetY: 2,
              shadowOffsetX: 2,
            },
            tooltip: {
              show: true,
            },
            areaStyle: {
              normal: {
                color: new echarts.graphic.LinearGradient(
                  0,
                  0,
                  0,
                  1,
                  [
                    {
                      offset: 0,
                      color: "rgba(0,202,149,0.3)",
                    },
                    {
                      offset: 1,
                      color: "rgba(0,202,149,0)",
                    },
                  ],
                  false
                ),
                shadowColor: "rgba(0,202,149, 0.9)",
                shadowBlur: 20,
              },
            },
            data: lineData[1],
          },
        ],
      };

      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    }
  }
  function initEchartMap() {
    let dataList = [
      { name: "北京", value: 21893095 },
      { name: "天津", value: 13866009 },
      { name: "河北", value: 74610235 },
      { name: "山西", value: 34915616 },
      { name: "内蒙古", value: 24049155 },
      { name: "辽宁", value: 42591407 },
      { name: "吉林", value: 24073453 },
      { name: "黑龙江", value: 31850088 },
      { name: "上海", value: 24870895 },
      { name: "江苏", value: 84748016 },
      { name: "浙江", value: 64567588 },
      { name: "安徽", value: 61027171 },
      { name: "福建", value: 41540086 },
      { name: "江西", value: 45188635 },
      { name: "山东", value: 101527453 },
      { name: "河南", value: 99365519 },
      { name: "湖北", value: 57752557 },
      { name: "湖南", value: 66444864 },
      { name: "广东", value: 126012510 },
      { name: "广西", value: 50126804 },
      { name: "海南", value: 10081232 },
      { name: "重庆", value: 32054159 },
      { name: "四川", value: 83674866 },
      { name: "贵州", value: 38562148 },
      { name: "云南", value: 47209277 },
      { name: "西藏", value: 3648100 },
      { name: "陕西", value: 39528999 },
      { name: "甘肃", value: 25019831 },
      { name: "青海", value: 5923957 },
      { name: "宁夏", value: 7202654 },
      { name: "新疆", value: 25852345 },
    ];
    let options = {
      tooltip: {
        triggerOn: "mousemove", //mousemove、click
        padding: 8,
        borderWidth: 1,
        borderColor: "#409eff",
        backgroundColor: "rgba(255,255,255,0.7)",
        textStyle: {
          color: "#000000",
          fontSize: 13,
        },
        formatter: function (e, t, n) {
          let data = e.data;
          let context = `
                <div>
                    <p><b style="font-size:15px;">${data.name}</b></p>
                    <p class="tooltip_style"><span class="tooltip_left">人数</span><span class="tooltip_right">${data.value}</span></p>
                    <p class="tooltip_style"><span class="tooltip_left">全国总人数</span><span class="tooltip_right">141177872</span></p>
                </div>
                `;
          return context;
        },
      },
      visualMap: {
        show: true,
        left: 26,
        bottom: 40,
        showLabel: true,
        textStyle: {
          color: "#ffffff",
          fontSize: 13,
        },
        pieces: [
          {
            gte: 100000000,
            label: ">= 1亿",
            color: "#484891",
          },
          {
            gte: 80000000,
            lt: 100000000,
            label: "8千万 - 1亿",
            color: "#7373B9",
          },
          {
            gte: 60000000,
            lt: 80000000,
            label: "6千万 - 8千万",
            color: "#9999CC",
          },
          {
            gte: 40000000,
            lt: 60000000,
            label: "4千万 - 6千万",
            color: "#B8B8DC",
          },
          {
            gte: 30000000,
            lt: 40000000,
            label: "3千万 - 4千万",
            color: "#D8D8EB",
          },
          {
            gte: 20000000,
            lt: 30000000,
            label: "2千万 - 3千万",
            color: "#F3F3FA",
          },
          {
            lt: 20000000,
            label: "<2千万",
            color: "#FAF4FF",
          },
        ],
      },
      geo: {
        map: "china",
        scaleLimit: {
          min: 1,
          max: 2,
        },
        zoom: 1,
        top: 120,
        label: {
          normal: {
            show: true,
            fontSize: "14",
            color: "rgba(0,0,0,0.7)",
          },
        },
        itemStyle: {
          normal: {
            //shadowBlur: 50,
            //shadowColor: 'rgba(0, 0, 0, 0.2)',
            borderColor: "rgba(0, 0, 0, 0.2)",
          },
          emphasis: {
            areaColor: "#f2d5ad",
            shadowOffsetX: 0,
            shadowOffsetY: 0,
            borderWidth: 0,
          },
        },
      },
      series: [
        {
          name: "突发事件",
          type: "map",
          geoIndex: 0,
          data: dataList,
        },
      ],
    };

    let myChart = echarts.init(document.getElementById("map_1"));
    myChart.setOption(options);
    window.addEventListener("resize", function () {
      myChart.resize();
    });
  }
});
