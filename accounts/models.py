from django.db import models


# Create your models here.
class population(models.Model):
    Year = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    country_name = models.CharField(max_length=64, db_column='Country name')
    Population = models.CharField(max_length=64)  # 人口
    Population_of_children_under_the_age_of_1 = models.CharField(max_length=64, db_column='Population of children under the age of 1')  # 1岁以下儿童人口
    Population_under_the_age_of_25 = models.CharField(max_length=64, db_column='Population under the age of 25')  # 25岁以下人口
    Population_aged_15_to_64_years = models.CharField(max_length=64, db_column='Population aged 15 to 64 years')  # 15至64岁人口
    Population_aged_90_to_99_years = models.CharField(max_length=64, db_column='Population aged 90 to 99 years')  # 15至64岁人口
    Population_older_than_100_years = models.CharField(max_length=64, db_column='Population older than 100 years')  # 15至64岁人口
    #  Population older than 15 years 15岁以上人口
