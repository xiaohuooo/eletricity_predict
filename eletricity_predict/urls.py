"""eletricity_predict URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from accounts.views import login_view, register_view,home,db_handle,echarts6,echarts1,echarts2,echarts31,echarts5,echarts4,map,countryList,countAll


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_view, name='login'),
    path('register/', register_view, name='register'),
    path('home/', home, name='home'),
    path('blogs/', db_handle, name='blogs'),
    path('echarts6/', echarts6, name='echarts6'),
    path('echarts1/', echarts1, name='echarts1'),
    path('echarts2/', echarts2, name='echarts2'),
    path('echarts31/', echarts31, name='echarts31'),
    path('echarts5/', echarts5, name='echarts5'),
    path('echarts4/', echarts4, name='echarts4'),
    path('map/', map, name='map'),
    path('countryList/', countryList, name='countryList'),
    path('countAll/', countAll, name='countAll')
]
